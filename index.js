const express = require("express"); //require will imports express module
const app = express(); //create application using express
const port = 3000; //listen to port 3000

app.use(express.json()); //use this setup for allowing the server to handle data from requests
						//allow the app to read json data
app.use(express.urlencoded({extended:true})); //URL Encoded allows the app to read data from forms


app.get("/",(req,res) => {
	res.send("Hello World!");
})

app.get("/hello",(req,res) => {
	res.send("Hello from the /hello endpoint!");
})

app.post("/hello",(req,res) => {
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
})

//Mock data base
let users = [];

app.post("/signup", (req, res) => {
	console.log(req.body);

	if(req.body.username !== '' && req.body.password !== ''){
		users.push(req.body);
		res.send(`User ${req.body.username} successfully registered`);
	}
	else {
		res.send("Please input BOTH username and password");
	}
})

app.put("/change-password", (req, res) => {

	let message;

	for(let i = 0; i < users.length; i++){

		if(req.body.username == users[i].username){

			users[i].password = req.body.password;
			message = `User ${req.body.username}'s password has been updated.`;
			break;
		}
		else{
			message = "User does not found";
		}
	}

	res.send(message);
})

//ACTIVITY SECTION

app.get("/home",(req,res) => {
	res.send("Welcome to the home page");
})

app.get("/users",(req,res) => {
	res.send(users);
})

app.delete("/delete-user", (req, res) => {

	let message;

	for(let i = 0; i < users.length; i++){

		if(req.body.username == users[i].username){

			users = users.filter(function(obj){
               return obj.username !== req.body.username;
        	});
			message = `User ${req.body.username}'s password has been deleted.`;
			break;
			
		}
		else{
			message = "User does not found";
		}
	}

	res.send(message);
})




app.listen(port, () => console.log(`Server is running at port ${port}`));
